var indexSectionsWithContent =
{
  0: "abcdegilmnoprstv",
  1: "c",
  2: "g",
  3: "bceiors",
  4: "abcdeilnprstv",
  5: "c",
  6: "es",
  7: "amrs"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Énumérations",
  7: "Valeurs énumérées"
};

